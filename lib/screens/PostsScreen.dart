import 'package:flutter/material.dart';
import 'package:my_flutter_test/main.dart';
import 'package:my_flutter_test/models/PostWithComment.dart';
import 'package:my_flutter_test/screens/CommentsScreen.dart';


class PostsScreen extends StatefulWidget {
  const PostsScreen({super.key});

  @override
  State<PostsScreen> createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {
  late Future<List<PostWithComments>> fpwc;

  @override
  void initState() {
    super.initState();
    fpwc = fetchPostsAndComments();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Posts screen',
      theme: ThemeData(primarySwatch: Colors.grey),
      home: Scaffold(
        appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            title: const Center(child: Text('Посты'),)
        ),
        body: Container(
          color: Colors.grey[300],
          child: FutureBuilder<List<PostWithComments>>(
            future: fpwc,
            builder: (context, snapshot) {
              if(snapshot.hasData) {
                return ListView.builder(
                    padding: const EdgeInsets.all(16),
                    itemCount: snapshot.data!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          color: Colors.grey[300],
                          child: GestureDetector(
                              onTap: () {
                                setPostId(index+1);
                                Navigator.push(context, MaterialPageRoute(builder: (context) => const CommentsScreen()));
                              },
                              child: Stack(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Container(
                                          padding: const EdgeInsets.all(10),
                                          color: Colors.white,
                                          child: Column(
                                              children: [
                                                Text(
                                                  snapshot.data![index].title,
                                                  style: const TextStyle(fontSize: 20),
                                                ),
                                                const Text(' '),
                                                Text(
                                                  snapshot.data![index].body,
                                                  style: const TextStyle(color: Colors.grey),
                                                )
                                              ]
                                          )
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: Container(
                                            height: 40,
                                            width: 40,
                                            color: Colors.red[700],
                                            child: Center(
                                              child: Text(
                                                snapshot.data![index].comments.length.toString(),
                                                style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 25
                                                ),
                                              ),
                                            )
                                        )
                                    ),
                                  )
                                ],
                              ),
                          )
                      );
                    }
                );
              } else if(snapshot.hasError) {
                return Center(child: Text('${snapshot.error}'));
              }
              return const Center(child:CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}