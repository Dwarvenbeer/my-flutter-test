import 'package:flutter/material.dart';
import 'package:my_flutter_test/models/PostWithComment.dart';
import 'package:my_flutter_test/screens/PostsScreen.dart';

import '../main.dart';
import '../models/Post.dart';

class PreloadScreen extends StatefulWidget {//todo no use
  const PreloadScreen({super.key});

  @override
  State<PreloadScreen> createState() => _PreloadScreenState();
}

class _PreloadScreenState extends State<PreloadScreen> {
  //late Future<List<PostWithComments>> fpwc;

  @override
  void initState() {
    super.initState();
    //fpwc = fetchPostsAndComments();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Preload Screen',
      theme: ThemeData(primarySwatch: Colors.grey),
      home: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            title: const Center(
                child: Text('Preload Screen')
            )
        ),
        body: Container(
          color: Colors.green,
          child: FutureBuilder<List<PostWithComments>>(
            future: fetchPostsAndComments(),
            builder: (context, snapshot) {

              if(snapshot.hasData) {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const PostsScreen()),
                );
                /*Navigator.pushNamed(
                  context,
                  '/posts',
                  //MaterialPageRoute(builder: (context) => const PostsScreen()),
                );*/
              } else if(snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}