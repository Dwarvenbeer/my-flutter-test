import 'package:flutter/material.dart';

import '../main.dart';
import '../models/Comment.dart';

class CommentsScreen extends StatefulWidget {
  const CommentsScreen({super.key});

  @override
  State<CommentsScreen> createState() => _CommentsScreenState();
}

class _CommentsScreenState extends State<CommentsScreen> {
  late List<Comment> comments;

  @override
  void initState() {
    super.initState();
    comments = getCommentsByPostId();
  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
        title: 'Comments screen',
        theme: ThemeData(primarySwatch: Colors.grey),
        home: Scaffold(
          appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.white,
              title: const Center(child: Text('Комментарии')),
              leading: IconButton(
                  onPressed: () {Navigator.pop(context);},
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  )
              ),
          ),
          body: Container(
            color: Colors.grey[300],
            child: ListView.builder(
                padding: const EdgeInsets.all(16),
                itemCount: comments.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    color: Colors.grey[300],
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                          height: 100,
                            padding: const EdgeInsets.all(10),
                            color: Colors.white,
                            child: Row(
                              children: [
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: Icon(
                                      Icons.account_circle,
                                      size: 50.0
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Column(
                                      children: [
                                        Text(
                                          comments[index].email,
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                        const Text(' '),
                                        Text(
                                          comments[index].name,
                                          style: const TextStyle(color: Colors.grey),
                                        )
                                      ]
                                  ),
                                )
                              ],
                            ),
                        ),
                      ),
                    ),
                  );
                }
            ),
          ),
        ),
      );
  }
}