import 'package:my_flutter_test/models/Comment.dart';

class PostWithComments {
  int userId;
  int id;
  String title;
  String body;
  List<Comment> comments;

  PostWithComments(
      this.userId,
      this.id,
      this.title,
      this.body,
      this.comments,
      );
}