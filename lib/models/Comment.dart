class Comment {
  int postId;
  int id;
  String name;
  String email;
  String body;

  Comment(
      this.postId,
      this.id,
      this.name,
      this.email,
      this.body,
      );

  factory Comment.fromJson(dynamic json) {
    return Comment(
        json['postId'] as int,
        json['id'] as int,
        json['name'] as String,
        json['email'] as String,
        json['body'] as String
    );
  }

  @override
  String toString() {
    return '{ $postId, $id, $name, $email, $body }';
  }
}