import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:pretty_http_logger/pretty_http_logger.dart';

import 'screens/PostsScreen.dart';
import 'models/Post.dart';
import 'package:my_flutter_test/models/Comment.dart';
import 'package:my_flutter_test/models/PostWithComment.dart';


void main() {
  runApp(const MaterialApp(
    title: 'Main block',
    home: PostsScreen(),
  ));
}

//logging middleware
HttpWithMiddleware http = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

late int postId;
late List<Post> posts;
late List<Comment> comments;
late List<PostWithComments> postsWithComments;

List<Post> getPosts() { return posts;}
List<Comment> getComments() { return comments;}
List<PostWithComments> getPostsWithComments() { return postsWithComments;}
List<Comment> getCommentsByPostId() {
  List<Comment> result = List.empty(growable: true);
  getPostsWithComments().forEach((pwc) {
    if(pwc.id == postId) result = pwc.comments;
  });
  return result;
}

void setPostId([i = 0]) {postId = i;}

Future<List<PostWithComments>> fetchPostsAndComments() async {
  //load posts
  final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
  if(response.statusCode == 200) {
    var postObjsJson = jsonDecode(response.body) as List;
    posts = postObjsJson.map((postJson) => Post.fromJson(postJson)).toList();
    //load comments if only posts loaded
    final response2 = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/comments'));
    if(response2.statusCode == 200) {
      var commentObjsJson = jsonDecode(response2.body) as List;
      comments = commentObjsJson.map((commentJson) => Comment.fromJson(commentJson)).toList();
      addCommentsToPosts();
      return postsWithComments;
    } else {
      throw Exception('Failed to load comments');
    }
  } else {
    throw Exception('Failed to load posts');
  }
}

void addCommentsToPosts() {
  List<PostWithComments> result = List.empty(growable: true);
  getPosts().forEach((post) {
    PostWithComments postWithComments = PostWithComments(post.userId, post.id, post.title, post.body, List.empty(growable: true));
    getComments().forEach((comment) {
      if(post.id == comment.postId) postWithComments.comments.add(comment);
    });
    result.add(postWithComments);
  });
  postsWithComments = result;
}

Future<List<Post>> fetchPosts() async {
  final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
  if(response.statusCode == 200) {
    var postObjsJson = jsonDecode(response.body) as List;
    posts = postObjsJson.map((postJson) => Post.fromJson(postJson)).toList();
    return posts;
  } else {
    throw Exception('Failed to load posts');
  }
}

Future<List<Comment>> fetchComments() async {
  final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/comments'));
  if(response.statusCode == 200) {
    var commentObjsJson = jsonDecode(response.body) as List;
    comments = commentObjsJson.map((commentJson) => Comment.fromJson(commentJson)).toList();
    return comments;
  } else {
    throw Exception('Failed to load comments');
  }
}